package com.androidstudy.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.androidstudy.repository.HomeRepository
import com.androidstudy.service.domain.PostModel
import com.androidstudy.service.domain.PostState
import com.androidstudy.service.toPostList
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(private val repo: HomeRepository) : ViewModel() {

    private val _postState = SingleLiveEvent<PostState>()
    val postState: LiveData<PostState> = _postState

    init {
        getAllProducts()
    }

    private fun getAllProducts() {
        _postState.postValue(PostState.Loading)

        viewModelScope.launch {
            try {
                val response = repo.getAllProducts().toPostList()
                _postState.postValue(PostState.PostSuccess(response))
            } catch (e: Exception) {
                _postState.postValue(PostState.PostFailure(e.message))
            }
        }
    }

}