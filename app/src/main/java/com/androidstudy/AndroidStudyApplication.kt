package com.androidstudy

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class AndroidStudyApplication : Application() {

    override fun onCreate() {
        super.onCreate()
    }
}