package com.androidstudy.service

import com.androidstudy.service.data.PostResponse
import retrofit2.Response
import retrofit2.http.GET

interface HomeService {

    @GET("todos")
    suspend fun getAllProducts(): Response<List<PostResponse>>
}