package com.androidstudy.service.domain

sealed class PostState {
    object Loading : PostState()
    class PostFailure(val errorMessage: String? = ""): PostState()
    class PostSuccess(val result: List<PostModel> = emptyList()) : PostState()
}