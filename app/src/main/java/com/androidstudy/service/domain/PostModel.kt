package com.androidstudy.service.domain

class PostModel(val id: Int, val title: String, val completed: Boolean)