package com.androidstudy.service

import com.androidstudy.service.data.PostResponse
import com.androidstudy.service.domain.PostModel

fun PostResponse.toModel() = PostModel(
    id = userId,
    title = title,
    completed = completed
)

fun List<PostResponse>.toPostList() = map { it.toModel() }