package com.androidstudy.service.data

data class PostResponse(
    val userId: Int,
    val title: String,
    val completed: Boolean
)