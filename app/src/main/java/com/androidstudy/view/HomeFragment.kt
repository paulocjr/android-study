package com.androidstudy.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.androidstudy.databinding.FragmentHomeBinding
import com.androidstudy.service.domain.PostModel
import com.androidstudy.service.domain.PostState
import com.androidstudy.view.adapter.PostAdapter
import com.androidstudy.viewmodel.HomeViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private val homeBinding get() = _binding!!
    private val homeViewModel: HomeViewModel by viewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return homeBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        homeViewModel.postState.observe(viewLifecycleOwner, handlerPostState())
    }

    private fun handlerPostState() = Observer<PostState> { result ->
        with(homeBinding) {
            when (result) {
                is PostState.Loading -> {
                    progressLoading.visibility = View.VISIBLE
                    rvItems.visibility = View.GONE
                }
                is PostState.PostSuccess -> {
                    progressLoading.visibility = View.GONE
                    preparePostAdapter(data = result.result)
                }
                is PostState.PostFailure -> {
                    progressLoading.visibility = View.GONE
                    rvItems.visibility = View.GONE
                }
            }
        }
    }

    private fun preparePostAdapter(data: List<PostModel>) {
        with(homeBinding) {
            rvItems.apply {
                visibility = View.VISIBLE
                adapter = PostAdapter(data)
                layoutManager = LinearLayoutManager(requireContext())
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}