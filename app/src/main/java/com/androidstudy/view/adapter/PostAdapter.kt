package com.androidstudy.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.androidstudy.R
import com.androidstudy.service.domain.PostModel

class PostAdapter(private val list: List<PostModel>) : RecyclerView.Adapter<PostAdapter.PostViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): PostViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.layout_post_item, viewGroup, false)
        return PostViewHolder(view)
    }

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {
        holder.bind(list[position])
    }

    override fun getItemCount(): Int = list.size

    inner class PostViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val tvId = view.findViewById<TextView>(R.id.tvId)
        private val tvTitle = view.findViewById<TextView>(R.id.tvTitle)
        private val tvConfirmed = view.findViewById<TextView>(R.id.tvConfirmed)

        fun bind(item: PostModel) {
            tvId.text = item.id.toString()
            tvTitle.text = item.title
            tvConfirmed.text = if (item.completed) "CONFIRMED" else "NOT CONFIRMED"
        }
    }
}