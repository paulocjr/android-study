package com.androidstudy.repository

import com.androidstudy.service.HomeService
import com.androidstudy.service.data.PostResponse
import javax.inject.Inject

class HomeRepository @Inject constructor(private val service: HomeService) {

    suspend fun getAllProducts(): List<PostResponse> {
        val response = service.getAllProducts()
        return if (response.isSuccessful) {
            response.body() ?: emptyList()
        } else {
            emptyList()
        }
    }
}